<?php
/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @copyright  Copyright (c) 2017 Bizcommerce
 */
class Mandae_Shipping_Model_Api
{
    const URL_API = 'https://api.mandae.com.br/v2/';
    const SANDBOX_URL_API = 'https://sandbox.api.mandae.com.br/v2/';

    protected $_helper;

    /**
     * @param $data
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @return Zend_Http_Response
     */
    public function shippingRate($data, Mandae_Shipping_Model_Carrier_Mandae $carrier)
    {
        $result = null;

        $postcode = $data['postcode'];
        $url = $this->_getUrl() . 'postalcodes/' . $postcode . '/rates';

        $declaredValue = $carrier->getConfigData('use_declared_value') ? $data['subtotal'] : '0.00';

        // e.g. { "declaredValue": 20.00, "weight" : 2, "height" : 100, "width" : 10,"length": 10 }
        $args = array(
            'declaredValue' => $declaredValue,
            'weight' => $data['weight'],
            'height' => $data['height'],
            'width' => $data['width'],
            'length' => $data['length'],
        );

        $data = new Varien_Object();
        $data->setData($args);

        $this->_getHelper()->log('E: ' . $url . '. DATA:' . json_encode($args));
        $rate = $this->_request($carrier, $url, 'POST', $data->getData());
        $this->_getHelper()->log('R:' . json_encode($rate));

        if (property_exists($rate, 'body')) {
            $result = json_decode($rate->getBody());
        }

        return $result;
    }


    /**
     *
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @return mixed
     */
    public function tracking(Mandae_Shipping_Model_Carrier_Mandae $carrier, $trackingCode)
    {
        $result = null;

        $url = $this->_getUrl() . 'trackings/' . $trackingCode;

        $this->_getHelper()->log('TRACKING E: '. $url);
        $tracking = $this->_request($carrier, $url);
        $this->_getHelper()->log('TRACKING R:' . json_encode($tracking));

        if (property_exists($tracking, 'body')) {
            $result = json_decode($tracking->getBody());
        }

        return $result;

    }

    /**
     * @param Mandae_Shipping_Model_Carrier_Mandae $carrier
     * @param $url
     * @param string $method
     * @param null $data
     *
     * @return Zend_Http_Response
     */
    protected function _request(Mandae_Shipping_Model_Carrier_Mandae $carrier, $url, $method = 'GET', $data = null)
    {
        $client = new Varien_Http_Client($url);
        $client->setMethod($method);
        $client->setHeaders(
            array(
                'Content-Type' => 'application/json',
                'Authorization' => $carrier->getConfigData('token')
            )
        );

        if ($data) {
            $client->setRawData(json_encode($data));
        }

        $response = $client->request();

        // initialize server and set URI
        return $response;
    }

    /**
     * @return Mandae_Shipping_Helper_Data|Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('mandae');
        }
        return $this->_helper;
    }

    protected function getStoreId()
    {
        return Mage::app()->getStore()->getStoreId();
    }

    protected function _getUrl()
    {
        return ($this->_getHelper()->isSandbox()) ? self::SANDBOX_URL_API : self::URL_API;
    }
}