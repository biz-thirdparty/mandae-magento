<?php

/**
 * Mandaê
 *
 * @category   Mandae
 * @package    Mandae_Shipping
 * @author     Thiago Contardi
 * @copyright  Copyright (c) 2017 Bizcommerce
 */
class Mandae_Shipping_Model_Source_Services
{
    public function toOptionArray()
    {
        /** @var Mandae_Shipping_Helper_Data $_helper */
        $_helper = Mage::helper('mandae');

        $options = array(
            'rapido' => $_helper->__('Fast'),
            'economico' => $_helper->__('Economy'),
            'expresso' => $_helper->__('Express')
        );

        $result = array();
        foreach ($options as $key => $option) {
            $result[] = array(
                'value' => $key,
                'label' => $option
            );
        }

        return $result;
    }
}