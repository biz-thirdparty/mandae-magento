# Mandaê shipping method module for Magento.

Shipping method module mandaê (v0.2.2) - Magento 1.7.x - 1.9.x


**[Mandaê](https://www.mandae.com.br/)** 

With this module you'll be able to:
 - Estimate Shipping Rates using **Mandaê**
 - Track your packages in Magento. 

## Installation

### Modgit: [https://github.com/jreinke/modgit](https://github.com/jreinke/modgit)

    $ cd /path/to/magento
    $ modgit init
    $ modgit add mandae-magento https://bitbucket.org/biz-thirdparty/mandae-magento.git

### Manual:

- Download the [latest version](https://bitbucket.org/biz-thirdparty/mandae-magento/downloads/) of the module.
- Unzip the file, copy and paste into yout Magento folder.
- Clear the Cache.
- Go to `System > Configuration > Shippping Methods Section`. There'll be a new section called "Mandaê".
- Fill your Mandaê Auth Token and that's it.


**[Documentação](https://dev.mandae.com.br/api/index.html)**
**[Dúvidas](https://mandae.zendesk.com/hc/pt-br/requests/new)**
